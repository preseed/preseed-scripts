## Preseed Scripts

Scripts to assist with the Debian preseeding process.

---

### filterdat

The questions and answers to a previous Debian install are found in:
* `/var/log/installer/cdebconf/templates.dat` 
* `/var/log/installer/cdebconf/questions.dat`

The `templates.dat` file had a bunch of entries that I didn't need so I figured it'd be a good opportunity to try my hand at `awk`.

#### Use
I created a directory in `$HOME` then copied the two files there and changed owners for easier processing.

```bash
# Create a directory
mkdir ~/preseed && cd ~/preseed
# Download the script
wget https://gitlab.com/preseed/preseed-scripts/raw/master/filterdat
# Make the script executable
chmod +x filterdat
# Copy the files to the new directory
sudo cp /var/log/installer/cdebconf/*.dat ~/preseed
# Change owner
sudo chown $USER ~/preseed/*.dat
# Run the script
./filterdat templates.dat
# Read the output
less ~/preseed/templates.filtered
```

---

You can also run the following commands for (more?) preseedable questions to filter through:
```
sudo debconf-get-selections --installer > filename
debconf-get-selections >> filename
```

---

### preseed2iso

This will take a configured preseed file and add it to a Debian iso for (almost) fully automated install. 

This method will also configure Networking.

---
